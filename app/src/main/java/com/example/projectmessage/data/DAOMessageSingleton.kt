package com.example.projectmessage.data

import com.example.projectmessage.model.Message
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.Calendar
import java.util.Date
import kotlin.random.Random

object DAOMessageSingleton {
    private var serial: Long = 1
    private val messages = ArrayList<Message>()


    fun add(m: Message){
        this.messages.add(m)
        m.id = serial++
    }

    fun getMessages(): ArrayList<Message>{
        return this.messages
    }
}