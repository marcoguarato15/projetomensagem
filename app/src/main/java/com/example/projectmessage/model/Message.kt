package com.example.projectmessage.model

data class Message (
    var sent: Boolean,
    var personName: String,
    var dateTimeMensagem: String,
    var message: String,
    var id: Long = 0
    )