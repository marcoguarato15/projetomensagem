package com.example.projectmessage.ui.list.viewholders

import android.view.View
import android.widget.TextView
import androidx.appcompat.view.menu.MenuView.ItemView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.projectmessage.R
import com.example.projectmessage.model.Message

class MessageViewHolder (
    itemView: View
        ): ViewHolder(itemView) {
            var tvPersonName = itemView.findViewById<TextView>(R.id.tvPersonName)

            var tvMessage = itemView.findViewById<TextView>(R.id.tvMessage)

            var tvTime = itemView.findViewById<TextView>(R.id.tvTime)

    private lateinit var currentMessage: Message

    fun bind(m: Message){
        this.currentMessage = m
        this.tvPersonName.text = m.personName
        this.tvTime.text = m.dateTimeMensagem
        this.tvMessage.text = m.message

    }


}