package com.example.projectmessage.ui.list.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.projectmessage.R
import com.example.projectmessage.model.Message
import com.example.projectmessage.ui.list.viewholders.MessageViewHolder

class MessageAdapter(
    private var listMessage: ArrayList<Message>
): Adapter<MessageViewHolder>() {
    companion object{
        private val TYPE_SENT = 0
        private val TYPE_RECEIVED = 1

    }
    override fun getItemViewType(position: Int): Int {
        if(listMessage[position].sent){
            return TYPE_SENT
        }else {
            return TYPE_RECEIVED
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            if(viewType == TYPE_SENT) {
                R.layout.itemview_sent_message_card
            }else{
                R.layout.itemview_received_message_card
        }, parent, false)
        val viewHolder = MessageViewHolder(itemView)
        return viewHolder
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        var message = this.listMessage[position]
        holder.bind(message)
    }

    override fun getItemCount(): Int {
        return listMessage.size
    }
}