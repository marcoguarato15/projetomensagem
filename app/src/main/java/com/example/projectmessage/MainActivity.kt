package com.example.projectmessage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectmessage.data.DAOMessageSingleton
import com.example.projectmessage.model.Message
import com.example.projectmessage.ui.list.adapters.MessageAdapter
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var etxtMessage: EditText
    private  lateinit var  rvMessageList: RecyclerView
    var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.etxtMessage = findViewById(R.id.etxtMessage)
        this.rvMessageList = findViewById(R.id.rvMessageList)
        this.rvMessageList.layoutManager = LinearLayoutManager(this)

        this.rvMessageList.adapter = MessageAdapter(
            DAOMessageSingleton.getMessages()
        )
    }

    fun onClickInsert(v: View){
        var text = this.etxtMessage.text.toString()
        if(text.trim() != "") {
            var date = LocalDateTime.now()
            var dateMessage = date.format(formatter).toString()
            var sent = Random.nextBoolean()
            var personName = ""
            if (sent) {
                personName = "Fulano"
            } else {
                personName = "Ciclano"
            }
            var m = Message(sent, personName, dateMessage, text)
            DAOMessageSingleton.add(m)
            this.rvMessageList.adapter?.notifyItemInserted(DAOMessageSingleton.getMessages().size - 1)
            this.etxtMessage.text.clear()
        }else{
            Toast.makeText(this, "Não há nada para enviar", Toast.LENGTH_SHORT).show()
        }
    }
}